package com.jjorda.carpetas;

import java.io.File;

public class Main {

	/** @param args */
	public static void main(String[] args) {
		if (args.length != 0) {
			String route = args[0];
			File directory = new File(route);
			if (directory.exists()) {
				extractFiles(directory);
			} else {
				System.out.println("El directorio" + directory + " no existe");
			}
		} else {
			System.out.println("Debes pasarme la ruta de la carpeta que quieres extraer de subcarpetas");
		}
		System.out.println("Acab�");

	}

	/**
	 * @param route
	 * @param directory
	 */
	protected static void extractFiles( File directory) {
		String route = directory.getAbsolutePath();
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				for (File fileAux : file.listFiles()) {
					if (fileAux.isDirectory()) {
						extractFiles(fileAux);
					} else {
						if (!fileAux.renameTo(new File(route + "\\" + fileAux.getName())))
							System.out.println("�No he podido mover " + fileAux.getName() + " desde " + fileAux.getAbsolutePath() + "!");
					}
				}
			}
		}
	}
}
